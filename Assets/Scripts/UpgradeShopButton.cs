﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeShopButton : MonoBehaviour
{

  public GameObject btn, UpgradeGUI;

  void OnTriggerEnter2D()
  {
      btn.SetActive(true);
  }

  void OnTriggerExit2D()
  {
      btn.SetActive(false);
  }

  public void OpenUpgradeMenu()
  {
    UpgradeGUI.SetActive(true);
  }

  public void CloseUpgradeMenu()
  {
    UpgradeGUI.SetActive(false);
  }
}
