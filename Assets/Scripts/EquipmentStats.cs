﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentStats : MonoBehaviour {

    public EquipmentData[] data;

    public EqLevel GetStat(PieceOfEquipment.EquipmentType eqType, int level)
    {
        foreach (EquipmentData eq in data)
        {
            if (eq.type == eqType)
            {
                if(level - 1 >= eq.stats.Length) { return null; }
                return eq.stats[level - 1];
            }
        }
        return null;
    }

    [System.Serializable]
    public class EquipmentData
    {
        public PieceOfEquipment.EquipmentType type;
        public EqLevel[] stats;
    }
    [System.Serializable]
    public class EqLevel
    {
        public int statValue;
        public int price;
        public Sprite eqSpriteCenter;
        public Sprite eqSpriteLeft; //a hack to get around separate sprites for right/left arm/leg
        public Sprite eqSpriteRight;
    }
}
