﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrencyManager : MonoBehaviour {

    public Data data;
    public Text currencyLabel;
    private int coins;
    public int Money
    {
        get
        {
            return coins;
        }
        private set
        {
            AddMoney(value);
        }
    }
    
    private void Awake()
    {
        coins = 0;
        coins = data.LoadCurrency();
        UpdateGUI();
    }
    public void AddMoney(int value)
    {
        coins += value;
        data.SaveCurrency(coins);
        UpdateGUI();
    }
    public void UpdateGUI()
    {
        currencyLabel.text = coins.ToString();
    }
    public bool Probe(int amount)
    {
        return amount <= coins;
    }

   
}
