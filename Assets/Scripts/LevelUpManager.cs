﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelUpManager : MonoBehaviour {

    private int _statPoints;
    public int StatPoints
    {
        get
        {
            return _statPoints;
        }
    }
    private Data _dataManager;
    public Player PlayerCharacter;
    public event Action OnStatPointsUpdated;

    private void Awake()
    {
        _dataManager = FindObjectOfType<Data>();
        _statPoints = _dataManager.LoadStatPoints();
    }
    public void TEST_passDataRef(Data data)
    {
        _dataManager = data;
    }
    public bool ProbeStr()
    {
        return StatPoints > 0 && PlayerCharacter.stats.strength < PlayerCharacter.stats.maxStr;
    }
    public void AddStatPoints(int amount)
    {
        _statPoints += amount;
        SaveStatCount();
    }
    public void UpgradeStr()
    {
        if (!ProbeStr()) { return; }
        _statPoints--;
        PlayerCharacter.UpdateStat(Character.StatType.STRENGTH, 1);
        if(OnStatPointsUpdated != null)
        {
            OnStatPointsUpdated();
        }
        SaveStatCount();
    }
    public bool ProbeDex()
    {
        return StatPoints > 0 && PlayerCharacter.stats.dexterity < PlayerCharacter.stats.maxDex;
    }
    public void UpgradeDex()
    {
        if (!ProbeDex()) { return; }
        _statPoints--;
        PlayerCharacter.UpdateStat(Character.StatType.DEXTERITY, 1);
        if (OnStatPointsUpdated != null)
        {
            OnStatPointsUpdated();
        }
        SaveStatCount();
    }
    public bool ProbeConst()
    {
        return StatPoints > 0 && PlayerCharacter.stats.constitution < PlayerCharacter.stats.maxConst;
    }
    public void UpgradeConst()
    {
        if (!ProbeConst()) { return; }
        _statPoints--;
        PlayerCharacter.UpdateStat(Character.StatType.CONSTITUTION, 1);
        PlayerCharacter.Heal(1);
        if (OnStatPointsUpdated != null)
        {
            OnStatPointsUpdated();
        }
        SaveStatCount();
    }
    void SaveStatCount()
    {
        _dataManager.SaveStatPoints(StatPoints);
    }
    //Intelligence currently doesnt really do anything
    //public bool ProbeInt()
    //{
    //    return true;
    //}
    //public void UpgradeInt()
    //{

    //}
}
