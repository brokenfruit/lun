﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform targetToFollow;
    private float offsetY;

    private void Awake()
    {
        offsetY = this.transform.position.y - targetToFollow.position.y;    
    }

    private void FixedUpdate()
    {
        this.transform.position = new Vector3(targetToFollow.position.x, targetToFollow.position.y + offsetY, this.transform.position.z);
    }

}
