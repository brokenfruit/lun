﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AnimationEventListener : MonoBehaviour {

    public event Action OnAttackAnimationEnded;
    public event Action EnableColliderTime;
    public event Action OnShieldUp;
    public event Action OnShieldDown;

    void AttackAnimEnded()
    {
        //Debug.Log("anim ended");
        OnAttackAnimationEnded();
    }
	void EnableColliderPoint()
    {
        EnableColliderTime();
    }
    void ShieldUp()
    {
        OnShieldUp();
    }
    void ShieldDown()
    {
        OnShieldDown();
    }
}
