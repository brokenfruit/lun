﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSounds : MonoBehaviour {

    AudioSource PlayerAudioSource;
    public AudioClip[] FootSteps;
    public AudioClip[] SwordSwings;
    public AudioClip[] Rolls;
    //public AudioClip[] Grunts;
    public Animator Anim;

    // Use this for initialization
    void OnEnable () {
        PlayerAudioSource = GetComponent<AudioSource>();
        Anim = GetComponent<Animator>();
        //if (Anim["Char_1_Move"].enabled == true)
        if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Char_1_Move"))
        {
            Randomize(FootSteps, 1F, 1.2F, 1.1F, 1.3F);
        }
        if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Char_1_Hit"))
        {
            Randomize(SwordSwings, 0.7F, 0.9F, 1.1F, 1.3F);
        }
        if (Anim.GetCurrentAnimatorStateInfo(0).IsName("Char_1_Roll"))
        {
            Randomize(Rolls, 0.5F, 0.6F, 1.1F, 1.3F);
        }


    }

    //void FootStepRandomize()
    //{
    //    AudioClip SoundToPlay = FootSteps[UnityEngine.Random.Range(0, FootSteps.Length)];
    //    PlayerAudioSource.volume = UnityEngine.Random.Range(1F, 1.2F);
    //    PlayerAudioSource.pitch = UnityEngine.Random.Range(1.1f, 1.3f);
    //    PlayerAudioSource.clip = SoundToPlay;
    //    PlayerAudioSource.Play();
    //}

    //void SwingRandomize()
    //{
    //    AudioClip SoundToPlay = SwordSwings[UnityEngine.Random.Range(0, SwordSwings.Length)];
    //    PlayerAudioSource.volume = UnityEngine.Random.Range(0.7F, 0.9F);
    //    PlayerAudioSource.pitch = UnityEngine.Random.Range(1.1f, 1.3f);
    //    PlayerAudioSource.clip = SoundToPlay;
    //    PlayerAudioSource.Play();
    //}

    //void RollsRandomize()
    //{
    //    AudioClip SoundToPlay = SwordSwings[UnityEngine.Random.Range(0, SwordSwings.Length)];
    //    PlayerAudioSource.volume = UnityEngine.Random.Range(0.7F, 0.9F);
    //    PlayerAudioSource.pitch = UnityEngine.Random.Range(1.1f, 1.3f);
    //    PlayerAudioSource.clip = SoundToPlay;
    //    PlayerAudioSource.Play();
    //}

    void Randomize(AudioClip[] Clips, float VolFrom, float VolTo, float PitchFrom, float PitchTo)
    {
        AudioClip SoundToPlay = Clips[UnityEngine.Random.Range(0, Clips.Length)];
        PlayerAudioSource.volume = UnityEngine.Random.Range(VolFrom, VolTo);
        PlayerAudioSource.pitch = UnityEngine.Random.Range(PitchFrom, PitchTo);
        PlayerAudioSource.clip = SoundToPlay;
        PlayerAudioSource.Play();
    }

    // Update is called once per frame
    void Start () {
        
    }
}
