﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Character {

    public bool isActive = true;
    //public float rollInputTimeWindow = 1f;
    public float rollDistance = 4;
    public float rollDuration = 1f;
    public int LevelUpRequirement = 100;
    //public float rollCooldown = 1.5f;
    //public Animator animator;
    public Text levelLabel;
    public GameObject LvlUpGUI;
    public LevelUpManager lvlUpMngr;
    public int Level
    {
        get
        {
            return level;
        }
    }
    public int Experience
    {
        get
        {
            return exp;
        }
    }

    private int level;
    private int exp;
    private bool clickLeft;
    private bool clickRight;
    private float timerForRollInput;
    private float rollCDtimer;
    private bool animWalkTriggered;
    private int rollDistanceModifier = 30;
    private int charLayer;
    private int npcLayer;
    private CurrencyManager cm;
    private Data _data;

    protected override void Awake()
    {
        _data = FindObjectOfType<Data>();
        cm = FindObjectOfType<CurrencyManager>();
        int hp = _data.LoadPlayerHP();
        stats.strength = _data.LoadPlayerStrength();
        stats.intelligence = _data.LoadPlayerIntelligence();
        stats.constitution = _data.LoadPlayerConstitution();
        stats.dexterity = _data.LoadPlayerDexterity();
        if (hp < 0) { RestoreHpToMax(); }else { healthInner = hp; }
        level = _data.LoadLevel();
        exp = _data.LoadExp();
        levelLabel.text = level.ToString();
        OnStatChange += StatChange;
        charLayer = LayerMask.NameToLayer("Characters");
        npcLayer = LayerMask.NameToLayer("NPCs");
        base.Awake();
    }
    public void TEST_passDataRef(Data data)
    {
        _data = data;
    }
    protected override void StatChange(StatType stat)
    {
        switch (stat)
        {
            case StatType.STRENGTH:
                _data.SavePlayerStrength(stats.strength);
                break;
            case StatType.INTELLIGENCE:
                _data.SavePlayerIntelligence(stats.intelligence);
                break;
            case StatType.CONSTITUTION:
                _data.SavePlayerConstitution(stats.constitution);
                break;
            case StatType.DEXTERITY:
                _data.SavePlayerDexterity(stats.dexterity);
                break;
            default:
                break;
        }
    }
    protected override void OnHpChange()
    {
        _data.SavePlayerHP(this.Health);
    }
    public void AddShieldBonus()
    {
        armor += eqController.shield.statValue;
    }
    public void RemoveShieldBonus()
    {
        armor -= eqController.shield.statValue;
    }
    private void StatChange()
    {
        rollDistance = rollDistanceModifier + (Mathf.Clamp(stats.dexterity / 3, 0, 5) * 5);
    }
    public void RegisterKill(int coinReward, int expReward)
    {
        //rn rewards are handled on player, this makes it really hard on 
        //customizing rewards so it should be moved to individual enemies
        cm.AddMoney(coinReward);
        AddExp(expReward);
    }
    public void AddExp(int amount)
    {
        exp += amount;
        if(exp >= LevelUpRequirement)
        {
            exp -= LevelUpRequirement;
            LevelUp();
        }
        _data.SaveExp(exp);
    }
    void LevelUp()
    {
        level++;
        levelLabel.text = level.ToString();
        _data.SaveLevel(level);
        lvlUpMngr.AddStatPoints(5);
        LvlUpGUI.SetActive(true); //veikia
        Time.timeScale = 0;
      ResetHealthLvlUp();

      //do more stuff
    }
    private void Update()
    {
        if (!isActive)
            return;

        timerForRollInput += Time.deltaTime;
        rollCDtimer -= Time.deltaTime;

        //CatchInputs();

        if (movingLeft)
        {
            Move(false);
        }
        else if (movingRight)
        {
            Move(true);
        }
    }

  public void MoneyCheat()
  {
    cm.AddMoney(100);
  }
    

    //void CatchInputs()
    //{
    //    if (Input.GetKeyDown(KeyCode.A))
    //    {
    //        MoveLeft(true);
    //    }else if (Input.GetKeyDown(KeyCode.D))
    //    {
    //        MoveRight(true);
    //    }else if (Input.GetKeyDown(KeyCode.Q))
    //    {
    //        RollLeft();
    //    }else if (Input.GetKeyDown(KeyCode.E))
    //    {
    //        RollRight();
    //    }

    //    if (Input.GetKeyUp(KeyCode.A))
    //    {
    //        MoveLeft(false);
    //    }
    //    if (Input.GetKeyUp(KeyCode.D))
    //    {
    //        MoveRight(false);
    //    }

    //    //if (movingLeft && !Input.GetKey(KeyCode.A))
    //    //{
    //    //    MoveLeft(false);
    //    //}
    //    //else if (movingRight && !Input.GetKey(KeyCode.D))
    //    //{
    //    //    MoveRight(false);
    //    //}
    //}

    //public void MoveLeft(bool initiate)
    //{
    //    if (!initiate)
    //    {
    //        movingLeft = false;
    //        if (!movingRight)
    //        {
    //            animator.SetTrigger("walkEnd");
    //        }
    //        return;
    //    }
    //    animator.SetTrigger("walkStart");
    //    movingLeft = true;
    //    movingRight = false;
    //    //Debug.Log("moveleft: ml -" + movingLeft + "; mr - " + movingRight + "; cl - " + clickLeft + "; cr - " + clickRight);

    //    if (clickLeft && timerForRollInput < rollInputTimeWindow)
    //    {
    //        RollLeft();
    //        clickLeft = false;
    //        clickRight = false;
    //    }
    //    else
    //    {
    //        clickLeft = true;
    //        clickRight = false;
    //        timerForRollInput = 0;
    //        if (facingRight)
    //        {
    //            Flip();
    //        }
    //        move(false);
    //    }
    //}

    //public void MoveRight(bool initiate)
    //{
    //    if (!initiate)
    //    {
    //        movingRight = false;
    //        if (!movingLeft)
    //        {
    //            animator.SetTrigger("walkEnd");
    //            animWalkTriggered = false;
    //        }
    //        return;
    //    }
    //    animator.SetTrigger("walkStart");
    //    movingRight = true;
    //    movingLeft = false;
    //    //Debug.Log("moveright: ml -" + movingLeft + "; mr - " + movingRight + "; cl - " + clickLeft + "; cr - " + clickRight);

    //    if (clickRight && timerForRollInput < rollInputTimeWindow)
    //    {
    //        RollRight();
    //        clickLeft = false;
    //        clickRight = false;
    //    }
    //    else
    //    {
    //        clickRight = true;
    //        clickLeft = false;
    //        timerForRollInput = 0;
    //        if (!facingRight)
    //        {
    //            Flip();
    //        }
    //        move(true);
    //    }
    //}

    public bool Roll(bool rollRight)
    {
        if (!isActive) { return false; }
        if (rollRight != facingRight)
        {
            Flip();
        }
        EnableInvincibility(rollDuration);
        StartCoroutine(Roll(rollDuration, rollRight ? rollDistance : -rollDistance));
        return true;
    }

    //void RollRight()
    //{
    //    Debug.Log("roll right");
    //    if (rollCDtimer >= 0)
    //    {
    //        return;
    //    }
        
    //    rollCDtimer = rollCooldown;
    //    if (!facingRight)
    //    {
    //        Flip();
    //    }
    //    animator.SetTrigger("roll");
    //    StartCoroutine(Roll(rollDuration, rollDistance));
    //}
    //void RollLeft()
    //{
    //    Debug.Log("roll left");
    //    if (rollCDtimer >= 0)
    //    {
    //        return;
    //    }
    //    rollCDtimer = rollCooldown;
    //    if (facingRight)
    //    {
    //        Flip();
    //    }
    //    animator.SetTrigger("roll");
    //    StartCoroutine(Roll(rollDuration, -rollDistance));
    //}
    IEnumerator Roll(float time, float distance)
    {
        isActive = false;
        movingRight = false;
        movingLeft = false;
        Physics2D.IgnoreLayerCollision(charLayer, npcLayer, true);
        float timer = 0;
        Vector2 nextPos = rbody.position;
        float endPosX = rbody.position.x + distance;
        while (timer < time)
        {
            nextPos.x += distance * (Time.fixedDeltaTime / time);
            nextPos.y = rbody.position.y;
            rbody.MovePosition(nextPos);
            if (timer + Time.fixedDeltaTime < time)
            {
                timer += Time.fixedDeltaTime;
                yield return new WaitForFixedUpdate();
            }else
            {
                rbody.MovePosition(new Vector2(endPosX, rbody.position.y));
                break;
            }
        }
        Physics2D.IgnoreLayerCollision(charLayer, npcLayer, false);
        isActive = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Mob")
        {
            this.TakeDamage(1,this.transform.position - collision.transform.position);
        }
    }

  

}
