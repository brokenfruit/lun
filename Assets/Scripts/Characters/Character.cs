﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Character : MonoBehaviour {

    public CharacterStats stats;
    public Rigidbody2D rbody;
    public int Health
    {
        get
        {
            return healthInner;
        }
        private set
        {
            healthInner = Mathf.Clamp(value, 0, MaxHealth);
            OnHpChange();
        }
    }
    public int MaxHealth
    {
        get
        {
            return stats.constitution;
        }
        private set
        {

        }
    }
    public int Damage;
    private float speedModifier = 1;
    private float speed;
    public float Speed
    {
        get
        {
            return speed * speedModifier;
        }        
    }
    public float Mana { get; private set; }
    public BoxCollider2D weaponCollider;
    public EquipmentController eqController;
    public int Armor
    {
        get
        {
            return armor;
        }
    }
    public bool IsInvincible
    {
        get
        {
            return isInvincible;
        }
    }
    
    public int armor;
    protected int healthInner;
    private int speedScaler = 40; //speed modifier to scale stats proprely
    private bool isInvincible;
    private float dmgInvincibilityTime = 0.2f;
    protected bool isGrounded;
    protected bool movingLeft;
    protected bool movingRight;
    protected bool facingRight = true;
    protected float knockbackX = 80;
    protected float knockbackY = 80;
    protected BoxCollider2D charCollider;
    private bool _isDead;
    public bool isDead
    {
        get
        {
            return _isDead;
        }
    }

    public event Action OnStatChange;
    public event Action OnHealthChange;
    public event Action OnDamageTaken;
    public event Action OnDeath;

    protected virtual void Awake()
    {
        RecalculateStats();
        charCollider = GetComponent<BoxCollider2D>();
    }
    protected virtual void Start()
    {
        CalculateArmor();
    }
    protected virtual void OnHpChange()
    {

    }
    public void AddToSpeedModifier(float value)
    {
        speedModifier += value;
    }

    /*public void OnShieldSpeedModifier()
    {
        speedModifier = speedModifier / 2;
    }
    public void OffShieldSpeedModifier()
    {
        speedModifier = speedModifier * 2;
    }*/
    public void CalculateArmor()
    {
        armor = 0;
        if(eqController == null) { return; }
        armor += eqController.armor == null ? 0 : eqController.armor.statValue;
        armor += eqController.boots == null ? 0 : eqController.boots.statValue;
        armor += eqController.helmet == null ? 0 : eqController.helmet.statValue;
        Debug.Log("armor: " + armor);
    }
    public void RecalculateStats()
    {
        Damage = Mathf.Clamp(stats.strength / 3, 1, 1000);
        if(eqController != null)
        {
            Damage += eqController.sword.statValue;
        }
        Mana = Mathf.Clamp(stats.intelligence / 3, 1, 1000);
        speed = (Mathf.Clamp(stats.dexterity / 3, 0, 10) * 5) + speedScaler;
        if(OnStatChange != null)
            OnStatChange();
    }
    public void TakeDamage(int amount, Vector2 direction)
    {
        if (isInvincible)
        {
            return;
        }
        amount = Mathf.Clamp(amount - armor, 0, int.MaxValue);
        if (amount > 0)
        {
            Health -= amount;
            //healthInner -= Mathf.Clamp((amount - armor), 0, int.MaxValue);
            StartCoroutine(takeDmg(amount, direction));
            EnableInvincibility(dmgInvincibilityTime);
        }
        if (OnDamageTaken != null) OnDamageTaken();
    }
    IEnumerator takeDmg(int amount, Vector2 direction)
    {
        //Debug.Log("take dmg - " + gameObject.name);
        if (IsCharAlive())
        {
            direction.Normalize();
            SuspendWalking();
            //Debug.Log(rbody.velocity);
            yield return new WaitForEndOfFrame();
            rbody.velocity = new Vector2(direction.x * knockbackX, knockbackY);
            //Debug.Log(rbody.velocity);
        }
        else
        {
            //ded
            StopAllCoroutines();
            _isDead = true;
            OnDeath();
            //this.gameObject.SetActive(false);
            //TODO: disable body collision with everything except environment
        }
    }
    public void ResetChar()
    {
        ChangeInvincState(false);
        RestoreHpToMax();
    }
    public void Heal(int amount)
    {
        Health += amount;
    }
    public void RestoreHpToMax()
    {
        Health = MaxHealth;
    }
    public void Move(bool moveRight)
    {
        if (!isGrounded)
        {
            return;
        }
        rbody.velocity = new Vector2(moveRight ? Speed : -Speed, rbody.velocity.y);
    }
    protected void Flip()
    {
        transform.localScale = new Vector3(transform.localScale.x*  -1, transform.localScale.y, transform.localScale.z);
        facingRight = !facingRight;
    }

     
    public void Attack()
    {
        weaponCollider.enabled = true;
    }
    public void disableWeaponCollider()
    {
        weaponCollider.enabled = false;
    }
    bool IsCharAlive()
    {
        if(Health <= 0)
        {
            //ded
            return false;
        }else
        {
            return true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            isGrounded = false;
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground" && isGrounded == false)
        {
            isGrounded = true;
        }
    }
    public void StopWalking()
    {
        movingLeft = false;
        movingRight = false;
    }
    void SuspendWalking()
    {
        rbody.velocity = new Vector2(rbody.velocity.x - (movingLeft ? -Speed : 0) - (movingRight ? Speed : 0), rbody.velocity.y);
    }
    public void StartMovement(bool moveRight)
    {
        if (moveRight)
        {
            movingRight = true;
        }
        else
        {
            movingLeft = true;
        }
        if (moveRight != facingRight)
        {
            Flip();
        }
    }
    private void Update()
    {
        if (movingLeft)
        {
            Move(false);
        }
        else if (movingRight)
        {
            Move(true);
        }
    }
    void ChangeInvincState(bool state)
    {
        isInvincible = state;
    }
    public void EnableInvincibility(float time)
    {
        if (!isInvincible)
        {
            StartCoroutine(TemporaryInvincibility(time));
        }
    }

    
    IEnumerator TemporaryInvincibility(float time)
    {
        ChangeInvincState(true);
        yield return new WaitForSeconds(time);
        ChangeInvincState(false);
    }
    public void UpdateStat(StatType stat, int amount)
    {
        switch (stat)
        {
            case StatType.STRENGTH:
                stats.strength += amount;
                StatChange(stat);
                break;
            case StatType.INTELLIGENCE:
                stats.intelligence += amount;
                StatChange(stat);
                break;
            case StatType.CONSTITUTION:
                stats.constitution += amount;
                StatChange(stat);
                break;
            case StatType.DEXTERITY:
                stats.dexterity += amount;
                StatChange(stat);
                break;
            default:
                break;
        }
    }
    protected virtual void StatChange(StatType stat)
    {
        
    }
    public enum StatType
    {
        STRENGTH,
        INTELLIGENCE,
        CONSTITUTION,
        DEXTERITY
    }
    [System.Serializable]
    public class CharacterStats
    {
        public int strength;
        public int intelligence;
        public int constitution;
        public int dexterity;
        public int maxStr;
        public int maxInt;
        public int maxConst;
        public int maxDex;     
    }

  public void ResetHealthLvlUp()
  {
    Health = MaxHealth;
  }
}
