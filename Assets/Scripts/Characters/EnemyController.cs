﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyController : MonoBehaviour {

    public Character character;
    public Animator animator;
    public AnimationEventListener animEvents;
    public Player player;
    public float leftBound;
    public float rightBound;
    //public bool generateAutoBounds;
    //public float distance;
    public float distanceToBoundThreshold;
    public float timeToWaitAtDestination;
    public int killRewardCoins;
    public int killRewardEXP;
    public State aiState = State.Patroling;
    public CollisionDetector playerDetector;
    public CollisionDetector playerInMeleeRangeDetector;
    public string spawnerID;
    public delegate void OnMobDeath(string ID);
    public event OnMobDeath Death;
    private bool isAttacking;
    AudioSource EnemyAudioSource;
    public AudioClip[] splashes;
    public enum State
    {
        Idle,
        Patroling,
        Fighting,
        Pursuing,
        Dead
    }

    private void Awake()
    {
    }
    private void OnEnable()
    {
        isAttacking = false;
        aiState = State.Patroling;
        StartCoroutine(Live());
        animEvents.OnAttackAnimationEnded += EndAttack;
        animEvents.EnableColliderTime += character.Attack;
        playerDetector.SubToCollision(SwitchToPursuit);
        playerDetector.SubToColliderLeaving(SwitchToPatrol);
        playerInMeleeRangeDetector.SubToCollision(SwitchToFighting);
        playerInMeleeRangeDetector.SubToColliderLeaving(SwitchToPursuit);
        character.OnDeath += HandleDeath;
        character.OnDamageTaken += PlayDmgSounds;
    }
    private void OnDisable()
    {
        StopCoroutine(Live());
        animEvents.OnAttackAnimationEnded -= EndAttack;
        animEvents.EnableColliderTime -= character.Attack;
        playerDetector.UnsubFromCollision(SwitchToPursuit);
        playerDetector.UnsubFromColliderLeaving(SwitchToPatrol);
        playerInMeleeRangeDetector.UnsubFromCollision(SwitchToFighting);
        playerInMeleeRangeDetector.UnsubFromColliderLeaving(SwitchToPursuit);
        character.OnDeath -= HandleDeath;
    }
    void HandleDeath()
    {
        Death(spawnerID);
        player.RegisterKill(killRewardCoins, killRewardEXP);
        //play death anim and upon its end disable this obj
        this.gameObject.SetActive(false);
    }
    void SwitchToPatrol()
    {
        if(aiState != State.Patroling)
        {
            character.StopWalking();
            aiState = State.Patroling;
            animator.SetTrigger("detectionOff");
        }
        
    }
    void SwitchToPursuit()
    {
        if (aiState != State.Pursuing)
        {
            aiState = State.Pursuing;
            animator.SetTrigger("detectionOn");
        }
    }
    void SwitchToFighting()
    {
        if(aiState != State.Fighting)
        {
            aiState = State.Fighting;
            character.StopWalking();
            //animator.SetTrigger("attack");
        }
    }
    IEnumerator Live()
    {
        int rndPath =  UnityEngine.Random.Range(0, 1);
        float bound = rndPath == 0 ? leftBound : rightBound;

        while (aiState != State.Dead)
        {
            switch (aiState)
            {
                case State.Idle:
                    animator.SetTrigger("walkEnd");
                    yield return new WaitForEndOfFrame();
                    break;
                case State.Patroling:
                    animator.SetTrigger("walkStart");
                    character.AddToSpeedModifier(-0.5f);
                    bound = bound == leftBound ? rightBound : leftBound;
                    WalkTowards(bound);
                    yield return StartCoroutine(CheckBound(bound));
                    character.StopWalking();
                    animator.SetTrigger("walkEnd");
                    float timer = timeToWaitAtDestination;
                    while (timer > 0 && aiState == State.Patroling)
                    {
                        yield return new WaitForSeconds(0.05f);
                        timer -= 0.05f;
                    }
                    character.AddToSpeedModifier(0.5f);
                    break;
                case State.Fighting:
                    if (!isAttacking)
                    {
                        isAttacking = true;
                        //character.Attack();
                        animator.SetTrigger("attack");
                    }
                    yield return new WaitForEndOfFrame();
                    break;
                case State.Pursuing:
                    WalkTowards(player.transform.position.x);
                    yield return new WaitForSeconds(0.01f);
                    //animator.SetTrigger("detectionOn");
                    //animator.SetTrigger("walkStart");
                    break;
                default:
                    yield return new WaitForEndOfFrame();
                    //animator.SetTrigger("walkStart");
                    break;
            }
        }

        yield break;
    }
    void EndAttack()
    {
        isAttacking = false;
        character.disableWeaponCollider();
    }
    void WalkTowards(float target)
    {
        float dir = target - this.transform.position.x;
        character.StartMovement(dir < 0 ? false : true);
    }
    IEnumerator CheckBound(float bound/*, CoroutineDelegate callback*/)
    {
        float dist = Mathf.Abs(bound - this.transform.position.x);
        while (aiState == State.Patroling && dist >= distanceToBoundThreshold)
        {
            yield return new WaitForEndOfFrame();
            //Debug.Log("     after end of frame yiel");
            dist = Mathf.Abs(bound - this.transform.position.x);
        }
        //Debug.Log("     reached destination or changed state");
        //callback();
        yield break;
    }

    public void PlayDmgSounds()
    {
        EnemyAudioSource = GetComponent<AudioSource>();
        //SplashRandomize();
        //Randomize(Grunts, 1.4F, 1.5F, 1.1F, 1.3F);
        //StartCoroutine(AudioPause());
        Randomize(splashes, 0.6F, 0.7F, 0.6F, 0.7F);
        //GruntRandomize(); 

    }

    void Randomize(AudioClip[] Clips, float VolFrom, float VolTo, float PitchFrom, float PitchTo)
    {
        if(Clips.Length <= 0)
        {
            return;
        }
        AudioClip SoundToPlay = Clips[UnityEngine.Random.Range(0, Clips.Length)];
        EnemyAudioSource.volume = UnityEngine.Random.Range(VolFrom, VolTo);
        EnemyAudioSource.pitch = UnityEngine.Random.Range(PitchFrom, PitchTo);
        EnemyAudioSource.clip = SoundToPlay;
        EnemyAudioSource.Play();
    }

}
