﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Player))]
public class PlayerController : MonoBehaviour {

    public Animator animator;
    public float rollInputTime;
    public float rollCooldown;
    public AnimationEventListener animEvents;
    AudioSource PlayerAudioSource;
    public AudioClip[] Grunts;
    public AudioClip[] Splashes;
    public AudioClip[] ShieldHits;
    public GameObject DeathGUI;  //this should be handled by another script altogether (GUI controller or game controller or sth like that
    public GameObject buttonRight, buttonLeft;

    private Player character;
    private bool waitingForInput;
    private bool rollCdActive;
    private bool lastClickRightSide;
    private bool rollActived;
    private bool isAttacking;
    private bool shieldStatus = true;
   

    private void Awake()
    {
        character = GetComponent<Player>();
        character.OnDeath += HandleDeath;
        animEvents.OnAttackAnimationEnded += EndAttack;
        animEvents.EnableColliderTime += character.Attack;
        animEvents.OnShieldDown += DisableShield;
        animEvents.OnShieldUp += ActivateShield;
        character.OnDamageTaken += PlayDmgSounds;
    }    
    void ActivateShield()
    {
        //add shield armor bonus
        //slow walking speed on shield
        character.AddShieldBonus();
        //character.OnShieldSpeedModifier();
        shieldStatus = false;
        //buttonLeft.interactable = false;
        //buttonRight.interactable = false;
        character.StopWalking();
    }
    void DisableShield()
    {
        //remove shield armor bonus
        //reset normal walking speed 
        character.RemoveShieldBonus();
        //character.OffShieldSpeedModifier();
        shieldStatus = true;
    }

  public void MoveRight(bool init)
  {
    if (shieldStatus)
    {
      Move(init, true);
    }
  }

  public void die()
      {
        DeathGUI.SetActive(true);
    }

    
    public void MoveLeft(bool init)
    {
      if (shieldStatus)
      {
            Move(init, false);
      }
         
    }
    public void Move(bool initiate, bool moveRight)
    {
        if (!initiate)
        {
            character.StopWalking();
            if (!rollActived)
            {
                animator.SetTrigger("walkEnd");
            }else
            {
                animator.ResetTrigger("walkEnd");
                rollActived = false;
            }
            return;
        }
        if (!waitingForInput && !rollCdActive)
        {
            StartCoroutine(WaitForRollInput());
            lastClickRightSide = moveRight;
        }else
        {
            if (moveRight == lastClickRightSide)
            {
                if (character.Roll(moveRight))
                {
                    animator.SetTrigger("roll");
                    StartCoroutine(RollCooldown());
                    rollActived = true;
                }
            }
            StopCoroutine(WaitForRollInput());
            return;
        }
        character.StartMovement(moveRight);
        animator.SetTrigger("walkStart");
    }
    IEnumerator WaitForRollInput()
    {
        //Debug.Log("Waiting for roll input");
        waitingForInput = true;
        float rollTimer = rollInputTime;
        while(rollTimer > 0)
        {
            rollTimer -= 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        waitingForInput = false;
        //Debug.Log("no longer waiting for roll input");
    }
    IEnumerator RollCooldown()
    {
        //Debug.Log("Roll cd is active");
        float timer = rollCooldown;
        rollCdActive = true;
        while(timer > 0)
        {
            timer -= 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        rollCdActive = false;
        //Debug.Log("Roll cd is no longer active");
    }
    public void Attack()
    {
        if (!isAttacking)
        {
            isAttacking = true;
            animator.SetTrigger("attack");
            //character.Attack();
            character.StopWalking();
        }
    }
    public void EndAttack()
    {
        isAttacking = false;
        character.disableWeaponCollider();
    }
    public void Shield(bool activate)
    {
        if (activate)
        {
            animator.SetTrigger("shieldUp");
        }else
        {
            animator.SetTrigger("shieldDown");
        }
    }

    void HandleDeath()
    {
        //play death anim
        this.gameObject.SetActive(false);
        DeathGUI.SetActive(true);
    }
    public void PlayDmgSounds()
    {
        PlayerAudioSource = GetComponent<AudioSource>();
        //SplashRandomize();
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Char_1_Shield_Hold"))
        {
            Randomize(ShieldHits, 0.6F, 0.7F, 1F, 1.1F);
        }
        else
        {
            Randomize(Grunts, 1.4F, 1.5F, 1.1F, 1.3F);
            StartCoroutine(AudioPause());
            Randomize(Splashes, 0.6F, 0.7F, 1F, 1.1F);
        }
        
        //GruntRandomize(); 

    } 

    //void GruntRandomize()
    //{
    //    AudioClip SoundToPlay = Grunts[UnityEngine.Random.Range(0, Grunts.Length)];
    //    PlayerAudioSource.volume = UnityEngine.Random.Range(1F, 1.2F);
    //    PlayerAudioSource.pitch = UnityEngine.Random.Range(1.1f, 1.3f);
    //    PlayerAudioSource.PlayOneShot(SoundToPlay);
    //}

    //void SplashRandomize()
    //{
    //    AudioClip SoundToPlay = Splashes[UnityEngine.Random.Range(0, Splashes.Length)];
    //    PlayerAudioSource.volume = UnityEngine.Random.Range(0.1F, 0.4F);
    //    PlayerAudioSource.pitch = UnityEngine.Random.Range(0.8f, 1.1f);
    //    PlayerAudioSource.PlayOneShot(SoundToPlay);
    //}

    void Randomize(AudioClip[] Clips, float VolFrom, float VolTo, float PitchFrom, float PitchTo)
    {
        AudioClip SoundToPlay = Clips[UnityEngine.Random.Range(0, Clips.Length)];
        PlayerAudioSource.volume = UnityEngine.Random.Range(VolFrom, VolTo);
        PlayerAudioSource.pitch = UnityEngine.Random.Range(PitchFrom, PitchTo);
        PlayerAudioSource.clip = SoundToPlay;
        PlayerAudioSource.Play();
    }

    IEnumerator AudioPause()
    {
        yield return new WaitForSeconds(0.2F);
    }
}
