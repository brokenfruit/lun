﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsMenuController : MonoBehaviour
{
  public GameObject StatsMenuGUI;

    public LevelUpManager lvlUpMngr;
    public GameObject[] dmgBar;
    public GameObject[] spdBar;
    public GameObject[] hpBar;

    public GameObject dmgButton;
    public GameObject spdButton;
    public GameObject hpButton;
    public Text spLabel;

    private Player.CharacterStats _stats;

    //needs to have references to all stat bars and buttons
    //then on OnEnabled probe'int visus status (lvlUpMngr.ProbeStr() ir t.t.)
    //ir enablint/disable'int atitinkamus mygtukus
    //also probe'int reik ant lvlUpMngr.OnStatPointsUpdated

    private void Awake()
    {
        lvlUpMngr.OnStatPointsUpdated += UpdateGUI;
        _stats = lvlUpMngr.PlayerCharacter.stats;
    }

    public void CloseStatsMenu()
  {
    StatsMenuGUI.SetActive(false);
    Time.timeScale = 1;
  }
    private void OnEnable()
    {
        if(_stats == null)
        {
            _stats = lvlUpMngr.PlayerCharacter.stats;
        }
        UpdateGUI();
    }
    void UpdateGUI()
    {
        for(int i = 0; i < _stats.strength; i++)
        {
            dmgBar[i].SetActive(true);
        }
        for(int i = 0; i < _stats.dexterity; i++)
        {
            spdBar[i].SetActive(true);
        }
        for(int i = 0; i < _stats.constitution; i++)
        {
            hpBar[i].SetActive(true);
        }
        spLabel.text = lvlUpMngr.StatPoints.ToString();

        dmgButton.SetActive(lvlUpMngr.ProbeStr());
        spdButton.SetActive(lvlUpMngr.ProbeDex());
        hpButton.SetActive(lvlUpMngr.ProbeConst());
    }

}
