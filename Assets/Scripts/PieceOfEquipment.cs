﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceOfEquipment : MonoBehaviour
{
    public enum EquipmentType
    {
        Sword,
        Helmet,
        Armor,
        Boots,
        Shield
    }
    public EquipmentType Type;
    public int eqLevel = 1;
    public int statValue;
    public bool playerItem;
    public SpriteRenderer center;
    public SpriteRenderer left;
    public SpriteRenderer right;
    private Data _data;
    private EquipmentStats.EqLevel itemLevelData;
    private CurrencyManager _cm;
    private EquipmentStats _eqStats;

    private void Awake()
    {
        _eqStats = FindObjectOfType<EquipmentStats>();
        _data = FindObjectOfType<Data>();
        _cm = FindObjectOfType<CurrencyManager>();
        if (playerItem)
        {
            eqLevel = _data.LoadEquipmentState(Type);
        }
        itemLevelData = _eqStats.GetStat(Type, eqLevel);
        statValue = itemLevelData.statValue;
        UpdateSprites();
    }
    private void Start()
    {
    }
    public void Upgrade()
    {
        itemLevelData = _eqStats.GetStat(Type, eqLevel + 1);
        if(itemLevelData == null)
        {
            Debug.Log("no more upgrades");
            return;
        }
        if (!_cm.Probe(itemLevelData.price))
        {
            return;
        }
        _cm.AddMoney(-itemLevelData.price);
        eqLevel++;
        statValue = itemLevelData.statValue;
        _data.SaveEquipmentState(Type, eqLevel);
        UpdateSprites();
    }
    void UpdateSprites()
    {
        if(center != null)
        {
            center.sprite = itemLevelData.eqSpriteCenter;
        }
        if(left != null)
        {
            left.sprite = itemLevelData.eqSpriteLeft;
        }
        if(right != null)
        {
            right.sprite = itemLevelData.eqSpriteRight;
        }
    }
}
