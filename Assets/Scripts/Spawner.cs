﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class Spawner : MonoBehaviour {

    public Player player;
    public List<SpawnPoint> spawns;

    private void Awake()
    {
        foreach(SpawnPoint pt in spawns)
        {
            pt.mob.GetComponent<EnemyController>().Death += OnDeath;
        }
        StartCoroutine(SpawnCycle());
    }
    IEnumerator SpawnCycle()
    {
        while (true)
        {
            foreach (SpawnPoint pt in spawns)
            {
                if (!pt.isAlive && pt.readyToSpawn && (player.transform.position.x < pt.leftBound || player.transform.position.x > pt.rightBound))
                {
                    pt.Spawn();
                }
            }
            yield return new WaitForSeconds(0.2f);
        }
    }
    public void OnDeath(string ID)
    {
        SpawnPoint pt = spawns.Find(x => x.ID == ID);
        pt.isAlive = false;
        StartCoroutine(deathCD(pt.deathCooldown, pt));
    }
    IEnumerator deathCD(float time, SpawnPoint pt)
    {
        yield return new WaitForSeconds(time);
        pt.readyToSpawn = true;
    }

    [Serializable]
    public class SpawnPoint
    {
        public string ID;
        public Vector3 position;
        public GameObject mob;
        public bool isAlive;
        public float leftBound;
        public float rightBound;
        public bool readyToSpawn;
        public float deathCooldown;
        
        public void Spawn()
        {
            mob.transform.position = position;
            mob.GetComponent<EnemyController>().leftBound = leftBound;
            mob.GetComponent<EnemyController>().rightBound = rightBound;
            mob.GetComponent<EnemyController>().spawnerID = ID;
            mob.SetActive(true);
            mob.GetComponent<EnemyController>().animator.SetTrigger("goIdle");
            mob.GetComponent<Character>().ResetChar();
            readyToSpawn = false;
            isAlive = true;
        }
    }
}
