﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsController : MonoBehaviour {

    
  public GameObject LvlUpGUI, StatsMenuGUI;

    public void OpenStatsMenu()
    {
      LvlUpGUI.SetActive(false);
      StatsMenuGUI.SetActive(true);

    }

    public void OpenStatsMenuLvl()
    {
        Time.timeScale = 0;
        StatsMenuGUI.SetActive(true);
    }
}
