﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public Character Owner;
    public ColliderTag Enemy;
    public enum ColliderTag{
        Mob,
        Player
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("trigger enter - " + other.name);
        if(other.tag == Enemy.ToString())
        {
            //Debug.Log("hit");
            other.GetComponent<Character>().TakeDamage(Owner.Damage,other.transform.position - Owner.transform.position);
        }
    }

}
