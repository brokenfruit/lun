﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour {

    public Data data;

	public void ResetGameState()
    {
        data.ResetData();
        SceneManager.LoadScene("game");
    }
}
