﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarScript : MonoBehaviour {
    private float fillAmount;
    public Image Content;
    public Player Player;
    public BarType barType;

    public enum BarType
    {
        HP,
        EXP
    }

	// Use this for initialization
	void Start () {
        //var player = GetComponent<Player>();

    }
	
	// Update is called once per frame
	void Update ()
	{
	    HandleBar();
	}

    private void HandleBar()
    {
        switch (barType)
        {
            case BarType.HP:
                Content.fillAmount = Map(Player.Health, 0, Player.MaxHealth, 0, 1);
                break;
            case BarType.EXP:
                Content.fillAmount = Map(Player.Experience, 0, Player.LevelUpRequirement, 0, 1);
                break;
            default:
                break;
        }
    }

    private float Map(float value, float inMin, float inMax, float outMin, float outMax)
    {
        return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }
}
