﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeWindowManager : MonoBehaviour {

    public EquipmentController eqController;
    public EquipmentStats eqStats;
	public Image shieldImg;
	public Text shieldPrice;
    public Image armorImg;
    public Image armorLeft;
    public Image armorRight;
    public Text armorPrice;
    public Image helmetImg;
    public Text helmetPrice;
    public Image bootsImgLeft;
    public Image bootsImgRight;
    public Text bootsPrice;
    public Image swordImg;
    public Text swordPrice;

    private void OnEnable()
    {
        UpdateWindow();
    }
    public void UpdateWindow()
    {
        EquipmentStats.EqLevel eqLvlData;
        eqLvlData = eqStats.GetStat(PieceOfEquipment.EquipmentType.Shield, eqController.shield.eqLevel + 1);
        if (eqLvlData != null)
        {
            shieldPrice.text = eqLvlData.price.ToString();
        }else{
			shieldPrice.text = "MAX LEVEL";
		}
		eqLvlData = eqStats.GetStat(PieceOfEquipment.EquipmentType.Shield, eqController.shield.eqLevel);
		shieldImg.sprite = eqLvlData.eqSpriteCenter;
        //--
        eqLvlData = eqStats.GetStat(PieceOfEquipment.EquipmentType.Armor, eqController.armor.eqLevel + 1);
        if (eqLvlData != null)
        {
            armorPrice.text = eqLvlData.price.ToString();
        }else{
			armorPrice.text = "MAX LEVEL";
		}
		eqLvlData = eqStats.GetStat(PieceOfEquipment.EquipmentType.Armor, eqController.armor.eqLevel);
		armorImg.sprite = eqLvlData.eqSpriteCenter;
		armorLeft.sprite = eqLvlData.eqSpriteLeft;
		armorRight.sprite = eqLvlData.eqSpriteRight;
        //--
        eqLvlData = eqStats.GetStat(PieceOfEquipment.EquipmentType.Helmet, eqController.helmet.eqLevel + 1);
        if (eqLvlData != null)
        {
            helmetPrice.text = eqLvlData.price.ToString();
        }else{
			helmetPrice.text = "MAX LEVEL";
		}
        eqLvlData = eqStats.GetStat(PieceOfEquipment.EquipmentType.Helmet, eqController.helmet.eqLevel);
		helmetImg.sprite = eqLvlData.eqSpriteCenter;
        //--
        eqLvlData = eqStats.GetStat(PieceOfEquipment.EquipmentType.Boots, eqController.boots.eqLevel + 1);
        if (eqLvlData != null)
        {
            bootsPrice.text = eqLvlData.price.ToString();
        }else{
			bootsPrice.text = "MAX LEVEL";
		}
        eqLvlData = eqStats.GetStat(PieceOfEquipment.EquipmentType.Boots, eqController.boots.eqLevel);
		bootsImgLeft.sprite = eqLvlData.eqSpriteLeft;
		bootsImgRight.sprite = eqLvlData.eqSpriteRight;
        //--
        eqLvlData = eqStats.GetStat(PieceOfEquipment.EquipmentType.Sword, eqController.sword.eqLevel + 1);
        if (eqLvlData != null)
        {
            swordPrice.text = eqLvlData.price.ToString();
        }else{
			swordPrice.text = "MAX LEVEL";
		}
		eqLvlData = eqStats.GetStat(PieceOfEquipment.EquipmentType.Sword, eqController.sword.eqLevel);
		swordImg.sprite = eqLvlData.eqSpriteCenter;
    }
}
