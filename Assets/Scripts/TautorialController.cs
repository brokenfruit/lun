﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TautorialController : MonoBehaviour
{

  public GameObject t1, t2, t3, t4, t5, t6;
  public GameObject b1, b2, tutorialWindow;
  private int i;

  public void NextPage()
  {

    if (t1.activeSelf)
    {
      t1.SetActive(false);
      t2.SetActive(true);
    }
    else if (t2.activeSelf)
    {
      t2.SetActive(false);
      t3.SetActive(true);  
    }
    else if (t3.activeSelf)
    {
      t3.SetActive(false);
      t4.SetActive(true);
    }
    else if (t4.activeSelf)
    {
      t4.SetActive(false);
      t5.SetActive(true);
    }
    else if (t5.activeSelf)
    {
      t5.SetActive(false);
      t6.SetActive(true);
      b1.SetActive(false);
      b2.SetActive(true);
    }
    }

  public void CloseTautorial()
  {
    tutorialWindow.SetActive(false);
  }
    
}

