﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentController : MonoBehaviour
{
    public Character ch;
    public PieceOfEquipment sword;
    public PieceOfEquipment helmet;
    public PieceOfEquipment armor;
    public PieceOfEquipment boots;
    public PieceOfEquipment shield;
    public UpgradeWindowManager upgWindow;
    
    public void UpgradeShield()
    {
        shield.Upgrade();
        ch.CalculateArmor();
        if (upgWindow != null) { upgWindow.UpdateWindow(); };
    }
    public void UpgradeSword()
    {
        sword.Upgrade();
        ch.RecalculateStats();
        if (upgWindow != null) { upgWindow.UpdateWindow(); };
    }
    public void UpgradeHelmet()
    {
        helmet.Upgrade();
        ch.CalculateArmor();
        if (upgWindow != null) { upgWindow.UpdateWindow(); };
    }
    public void UpgradeArmor()
    {
        armor.Upgrade();
        ch.CalculateArmor();
        if (upgWindow != null) { upgWindow.UpdateWindow(); };
    }
    public void UpgradeBoots()
    {
        boots.Upgrade();
        ch.CalculateArmor();
        if (upgWindow != null) { upgWindow.UpdateWindow(); };
    }
}
