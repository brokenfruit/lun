﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{
    string equipmentPrefix = "eqState";
    string currencyKey = "coins";
    string playerHealthKey = "plHP";
    string playerStatPrefix = "plstat_";
    string strength = "str";
    string intelligence = "int";
    string constitution = "cont";
    string dexterity = "dex";
    string playerExp = "xp";
    string playerLvl = "lvl";
    string statPtCount = "statpts";

    private void Awake()
    {
    }
    public void SaveStatPoints(int amount)
    {
        PlayerPrefs.SetInt(statPtCount, amount);
        PlayerPrefs.Save();
    }
    public int LoadStatPoints()
    {
        return PlayerPrefs.GetInt(statPtCount, 0);
    }
    public void SaveExp(int amount)
    {
        PlayerPrefs.SetInt(playerExp, amount);
        PlayerPrefs.Save();
    }
    public int LoadExp()
    {
        return PlayerPrefs.GetInt(playerExp, 0);
    }
    public void SaveLevel(int lvl)
    {
        PlayerPrefs.SetInt(playerLvl, lvl);
        PlayerPrefs.Save();
    }
    public int LoadLevel()
    {
        return PlayerPrefs.GetInt(playerLvl, 1);
    }
    public void SavePlayerStrength(int amount)
    {
        PlayerPrefs.SetInt(playerStatPrefix + strength, amount);
        PlayerPrefs.Save();
    }
    public int LoadPlayerStrength()
    {
        return PlayerPrefs.GetInt(playerStatPrefix + strength, 3);
    }
    public void SavePlayerIntelligence(int amount)
    {
        PlayerPrefs.SetInt(playerStatPrefix + intelligence, amount);
        PlayerPrefs.Save();
    }
    public int LoadPlayerIntelligence()
    {
        return PlayerPrefs.GetInt(playerStatPrefix + intelligence, 3);
    }
    public void SavePlayerConstitution(int amount)
    {
        PlayerPrefs.SetInt(playerStatPrefix + constitution, amount);
        PlayerPrefs.Save();
    }
    public int LoadPlayerConstitution()
    {
        return PlayerPrefs.GetInt(playerStatPrefix + constitution, 10);
    }
    public void SavePlayerDexterity(int amount)
    {
        PlayerPrefs.SetInt(playerStatPrefix + dexterity, amount);
        PlayerPrefs.Save();
    }
    public int LoadPlayerDexterity()
    {
        return PlayerPrefs.GetInt(playerStatPrefix + dexterity, 6);
    }
    public void SavePlayerHP(int amount)
    {
        PlayerPrefs.SetInt(playerHealthKey, amount);
        PlayerPrefs.Save();
    }
    public int LoadPlayerHP()
    {
        return PlayerPrefs.GetInt(playerHealthKey, -1);
    }
    public void SaveEquipmentState(PieceOfEquipment.EquipmentType type, int level)
    {
        PlayerPrefs.SetInt(equipmentPrefix + type, level);
        PlayerPrefs.Save();
    }
    public int LoadEquipmentState(PieceOfEquipment.EquipmentType type)
    {
        return PlayerPrefs.GetInt(equipmentPrefix + type, 1);
    }
    public void SaveCurrency(int value)
    {
        PlayerPrefs.SetInt(currencyKey, value);
        PlayerPrefs.Save();
    }
    public int LoadCurrency()
    {
        return PlayerPrefs.GetInt(currencyKey, 0);
    }
    public void ResetData()
    {
        // BE CAREFUL, ONLY TEMPORARY SOLUTION
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }
}
