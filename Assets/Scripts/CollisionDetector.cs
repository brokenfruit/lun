﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CollisionDetector : MonoBehaviour {

    public string TagToCheckAgainst;

    private event Action collisionDetected;
    private event Action colliderLeft;

    public void SubToColliderLeaving(Action arg)
    {
        colliderLeft += arg;
    }
    public void UnsubFromColliderLeaving(Action arg)
    {
        colliderLeft -= arg;
    }

    public void SubToCollision(Action arg)
    {
        collisionDetected += arg;
    }
    public void UnsubFromCollision(Action arg)
    {
        collisionDetected -= arg;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("trigger enter - " + other.name);
        if (other.tag == TagToCheckAgainst)
        {
            //Debug.Log("hit");
            if (collisionDetected != null)
            {
                collisionDetected();
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == TagToCheckAgainst)
        {
            if(colliderLeft != null)
            {
                colliderLeft();
            }
        }
    }

}
