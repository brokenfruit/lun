﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class StatCapTest
{
    //Tests wether character heals upon leveling up
    [Test]
    public void EditorTest()
    {
        //Arrange
        var gameObject = new GameObject();
        Data data = gameObject.AddComponent<Data>();
        Player plr = gameObject.AddComponent<Player>();
        gameObject.AddComponent<BoxCollider2D>();
        LevelUpManager lvlMng = gameObject.AddComponent<LevelUpManager>();
        lvlMng.TEST_passDataRef(data);
        plr.TEST_passDataRef(data);
        plr.stats = new Character.CharacterStats();
        plr.stats.dexterity = 1;
        plr.stats.maxDex = 1;
        plr.stats.strength = 1;
        plr.stats.maxStr = 1;
        plr.stats.constitution = 1;
        plr.stats.maxConst = 1;
        plr.RecalculateStats();
        lvlMng.AddStatPoints(3);
        lvlMng.PlayerCharacter = plr;

        //Act
        //Try to rename the GameObject
        lvlMng.UpgradeDex();
        lvlMng.UpgradeConst();
        lvlMng.UpgradeStr();

        //Assert
        //The object has a new name
        Assert.IsFalse(plr.stats.dexterity > plr.stats.maxDex);
        Assert.IsFalse(plr.stats.strength > plr.stats.maxStr);
        Assert.IsFalse(plr.stats.constitution > plr.stats.maxConst);
    }
}
