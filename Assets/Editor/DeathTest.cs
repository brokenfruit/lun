﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class DeathTest {
    //Tests wether character dies upon taking fatal damage
	[Test]
	public void EditorTest() {
		//Arrange
		var gameObject = new GameObject();
        gameObject.AddComponent<Character>();
        gameObject.AddComponent<BoxCollider2D>();
        Character ch = gameObject.GetComponent<Character>();
        ch.stats = new Character.CharacterStats();

        //Act
        //Try to rename the GameObject

        ch.stats.constitution = 1;
        ch.RecalculateStats();
        ch.TakeDamage(1, Vector2.left);

        //Assert
        //The object has a new name
        Assert.IsTrue(ch.isDead);
	}
}
